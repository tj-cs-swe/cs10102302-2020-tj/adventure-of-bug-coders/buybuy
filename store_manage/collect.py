from account_manage.login import login_required
from store_manage.store_models import Product
from account_manage.account_models import User
from db_manage.sql import db
from sqlalchemy import and_
from flask import request, render_template, session, Blueprint
import sys
sys.path.append("..")

# 跨文件路由需要蓝图
collect_app = Blueprint('Collect', __name__)

#=========================================================
# 辅助函数
# 1.0展示商品列表，用于收藏夹和浏览记录
def show_list(user_list):  
    # 如果收藏夹为空，则返回空列表 
    prod_list = []  
    while '' in user_list:
        user_list.remove('')
    for prod_id in user_list:
        prod_obj = Product.query.filter(and_((Product.id == int(prod_id)), Product.identity_ok)).first()
        if prod_obj is not None:
            prod_list.append(prod_obj)
    prod_list.reverse()  
    return prod_list

# 2.0删除商品，用于收藏夹和浏览记录
def del_list(del_list, user, choice):
    if choice==1:
        # 获取数据库中收藏夹的列表
        user_list = user.collect_list.split('_') 
    elif choice==2:
        user_list = user.browse_list.split('_')
    for del_prod in del_list:
        if del_prod in user_list:
            user_list.remove(del_prod)
    while '' in user_list:
        user_list.remove('')
    # 以下在数据库中重新建立浏览记录
    if choice==1:
        user.collect_list = ""
        for prod in user_list:
            user.collect_list = user.collect_list + '_' + prod
    elif choice==2:
        user.browse_list = ""
        for prod in user_list:
            user.browse_list = user.browse_list + '_' + prod
    # 以下从数据库中读取浏览记录中的商品信息
    prod_list = []
    for item_id in user_list:
        item_obj = Product.query.filter(and_((Product.id == int(item_id)), Product.identity_ok)).first()
        if item_obj is not None:
            prod_list.append(item_obj)
    prod_list.reverse()
    db.session.commit()
    return prod_list
#=========================================================
#路由

# 1. 添加收藏
@collect_app.route('/add_my_collect/<prod_id>', methods=['GET', 'POST'])
@login_required
def add_my_collect(prod_id):
    '''添加收藏：将商品的id添加到User的collect_list中'''
    prod = Product.query.filter_by(id=prod_id).first()  # 取得要查看的商品
    # 将商品id添加到collect_list中
    username = session.get('username')
    user = User.query.filter(User.username == username).first()
    seller=User.query.filter(User.username==prod.uploader_name).first()
    if user is not None:
        if user.collect_list is None:
            user.collect_list = str(prod_id)
        else:
            prod_collect_list = user.collect_list.split("_")
            if str(prod_id) in prod_collect_list:
                # 如果之前存在，那么将其删除再更新到尾巴
                prod_collect_list.remove(str(prod_id))
                prod_collect_list.append(str(prod_id))
            else:  # 如果之前不存在，那么判断已浏览的个数
                if len(prod_collect_list) >= 40:
                    prod_collect_list.pop(0)
                prod_collect_list.append(str(prod_id))
            user.collect_list = ""
            for prod_collect in prod_collect_list:
                user.collect_list = user.collect_list + "_" + prod_collect
        db.session.commit()
    return render_template('show_prod.html', key_word='0', 
                           prod=prod, user_id=user.id, seller_id=seller.id,
                           prod_id=prod_id, username=session.get('username'), is_collect=1)

# 2 .查看收藏夹
@collect_app.route('/my_collect', methods=['GET', 'POST'])
@login_required
def my_collect():
    '''查看收藏夹：根据collect_list从数据库中获得收藏的商品的数组传递给前端'''
    username = session.get('username')
    user = User.query.filter(User.username == username).first()
    prod_list = []  # 如果收藏夹为空，则返回空列表
    if user.collect_list is not None:  # 如果收藏列表不为空，添加列表内容
        collect_list = user.collect_list.split('_')
        prod_list = show_list(collect_list)  
    return render_template('my_collect.html', prod_list=prod_list, username=user.username)

# 3.删除收藏夹收藏记录
@collect_app.route('/edit_my_collect', methods=['GET', 'POST'])
@login_required
def del_my_collect():
    '''删除收藏记录'''
    if request.method == 'POST':
        del_prod_list = request.form.getlist("sel_prod")  # 从表单中获取删除记录商品的id列表
        username = session.get('username')
        user = User.query.filter(User.username == username).first()
        prod_list = del_list(del_prod_list, user, 1)
        # 获取数据库中收藏夹的列表
        user_collect_list = user.collect_list.split('_') 
        if user_collect_list:
            return render_template('del_my_collect.html', prod_list=prod_list, username=user.username)
        else:
            return render_template('my_collect.html', prod_list=prod_list, username=user.username)
    # 以下为初次从查看浏览记录页面切换到编辑浏览页面的部分
    # 初次进入编辑页面，从数据库读取浏览记录并显示
    else:
        username = session.get('username')
        user = User.query.filter(User.username == username).first()
        prod_list = []  # 如果收藏夹为空，则返回空列表
        if user.collect_list is not None:
            collect_list = user.collect_list.split('_')
            prod_list = show_list(collect_list)
        return render_template('del_my_collect.html', prod_list=prod_list, username=user.username)