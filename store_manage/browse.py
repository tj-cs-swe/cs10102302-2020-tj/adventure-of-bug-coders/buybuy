from account_manage.login import login_required
from store_manage.store_models import Product
from account_manage.account_models import User
from db_manage.sql import db
from sqlalchemy import and_
from flask import request, render_template, session, Blueprint
from store_manage.collect import show_list
from store_manage.collect import del_list
import sys

sys.path.append("..")

# 跨文件路由需要蓝图
browse_app = Blueprint('Browse', __name__)

#========================================================
#路由

#  1.查看浏览记录
@browse_app.route('/my_browse', methods=['GET', 'POST'])
@login_required
def my_browse():
    '''查看浏览记录：将User中的browse_list分开得到浏览过的商品的id数组，然后将这些商品信息传回给前端'''
    username = session.get('username')
    user = User.query.filter(User.username == username).first()
    prod_list = []  # 如果浏览记录为空，则返回空列表
    if user.browse_list is not None:  # 如果浏览记录不为空，添加列表内容
        browse_list = user.browse_list.split('_')
        prod_list = show_list(browse_list)  
    return render_template('my_browse.html', prod_list=prod_list, username=user.username)

#   2.删除浏览记录
#   在查看浏览记录的网页my_browse.html中添加了一个超链接，该链接指向/edit_my_browse
#   初次点击链接会调用函数并跳转到新的网页del_my_browse.html，这个网页只有[删除浏览记录]的作用
#   该网页用表单form提交信息给后台，用input的checkbox属性组成复选框，选择要删除的浏览记录
#   提交表单时，表单用post方法提交到edit_my_browse
#   要求form中属性为checkbox的input 的name是sel_prod
@browse_app.route('/edit_my_browse', methods=['GET', 'POST'])
@login_required
def del_my_browse():
    '''
    删除浏览记录：修改User的browse_list，
    先将浏览记录的全部商品信息传给前端，然后接受前端的删除记录的数组，在后端进行修改
    '''
    if request.method == 'POST':
        del_prod_list = request.form.getlist("sel_prod")  # 从表单中获取删除记录商品的id列表
        username = session.get('username')
        user = User.query.filter(User.username == username).first()
        prod_list = del_list(del_prod_list, user, 2)
        # 获取数据库中浏览记录的列表
        user_browse_list = user.browse_list.split('_')
        if user_browse_list:
            return render_template('del_my_browse.html', prod_list=prod_list, username=user.username)
        else:
            return render_template('my_browse.html', prod_list=prod_list, username=user.username)
    # 以下为初次从查看浏览记录页面切换到编辑浏览页面的部分
    # 初次进入编辑页面，从数据库读取浏览记录并显示
    else:
        username = session.get('username')
        user = User.query.filter(User.username == username).first()
        prod_list = []  # 如果浏览记录为空，则返回空列表
        if user.browse_list is not None:  # 如果浏览记录不为空，添加列表内容
            browse_list = user.browse_list.split('_')
            prod_list = show_list(browse_list)  
        return render_template('del_my_browse.html', prod_list=prod_list, username=user.username)
