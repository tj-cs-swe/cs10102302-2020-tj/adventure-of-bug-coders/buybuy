﻿from account_manage.login import login_required
from store_manage.store_models import Product
from account_manage.account_models import User
from db_manage.sql import db
from sqlalchemy import and_
from flask import request, render_template, session, Blueprint
import sys

sys.path.append("..")

# 跨文件路由需要蓝图
check_store_app = Blueprint('Check_store', __name__)

def get_founded_prod(keyword_list):
    prod_found = set()
    #通过不同的被分割的关键字进行查找
    for keyword_name in keyword_list:
        prod_this_keyword = Product.query.filter(
            and_(Product.name.like("%" + keyword_name + "%"), Product.identity_ok)).all()
        for prod in prod_this_keyword:
            prod_found.add(prod)
    return prod_found        
#==========================================================
#路由
# 1.搜索商品
@check_store_app.route('/getsearch', methods=['GET', 'POST'])
@check_store_app.route('/getsearch/<path:key_word>', methods=['GET', 'POST'])
@login_required
def get_prod(key_word=''):
    '''搜索商品：返回符合关键字的商品的列表'''
    if request.method == 'POST':
        keyword = request.form.get('keyword')
        keyword_list = keyword.split()  # 把整个字符串按照空格分成各个列表，分割关键字
        # 利用set去重
        prod_found = get_founded_prod(keyword_list)               
        return render_template('search_result.html',
                               keyword=keyword,
                               username=session.get('username'),
                               prod_list=list(prod_found))
    else:
        keyword_list = key_word.split()  # 把整个字符串按照空格分成各个列表，分割关键字
        # 利用set去重
        prod_found = get_founded_prod(keyword_list)    
        return render_template('search_result.html',
                               keyword=key_word,
                               username=session.get('username'),
                               prod_list=list(prod_found))

# 2.显示商品详情页
@check_store_app.route('/show_item', methods=['GET', 'POST'])
@check_store_app.route('/show_item/<key_word>/<prod_id>', methods=['GET', 'POST'])
@login_required
def show_prod(key_word, prod_id):
    '''显示商品：显示商品详情，并将商品添加到浏览记录中'''
    prod = Product.query.filter_by(id=prod_id).first()  # 取得要查看的商品
    # 将商品id添加到browse_list中
    username = session.get('username')
    user = User.query.filter(User.username == username).first()
    # 判断商品是否在收藏夹里
    is_collect = 0
    if user.collect_list is not None:
        prod_collect_list = user.collect_list.split("_")
        if str(prod.id) in prod_collect_list:
            is_collect = 1
    if user.browse_list is None:
        user.browse_list = str(prod_id)
    else:
        prod_browse_list = user.browse_list.split("_")
        if str(prod_id) in prod_browse_list:
            # 如果之前存在，那么将其删除再更新到尾巴
            prod_browse_list.remove(str(prod_id))
            prod_browse_list.append(str(prod_id))
        else:  
            # 如果之前不存在，那么判断已浏览的个数
            if len(prod_browse_list) >= 10:
                prod_browse_list.pop(0)
            prod_browse_list.append(str(prod_id))

        user.browse_list = ""
        for prod_browse in prod_browse_list:
            user.browse_list = user.browse_list + "_" + prod_browse
    db.session.commit()
    seller = User.query.filter(User.username == prod.uploader_name).first()
    return render_template('show_prod.html', key_word=key_word, prod=prod, username=session.get('username'),
                           is_collect=is_collect, user_id=user.id, seller_id=seller.id, prod_id=prod.id)

# 3.查看商品列表
@check_store_app.route('/myprod', methods=['GET', 'POST'])
@login_required
def my_prod():
    '''查看商品列表：后端向前端发送本人创建的所有商品'''
    username = session.get('username')
    user = User.query.filter(User.username == username).first()
    #通过用户名查找创建者为本人的商品
    prod_list = Product.query.filter(
        Product.uploader_name == user.username).all()
    return render_template('my_prod.html', prod_list=prod_list, username=user.username)