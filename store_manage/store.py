from account_manage.login import login_required
from store_manage.store_models import Product
from account_manage.account_models import User
from chat_manage.chat_models import Message
from db_manage.sql import db
from flask import request, render_template, redirect, url_for, flash, session, Blueprint
from account_manage.admin_account import ALLOWED_EXTENSIONS
from account_manage.admin_account import allowed_file
import os
import sys
import uuid

sys.path.append("..")

#========================================================================
# 跨文件路由需要蓝图
store_app = Blueprint('store', __name__)

#===========================================================================
# 路由

#1.添加商品
@store_app.route('/add_product', methods=['GET', 'POST'])
@login_required
def add_product():
    '''添加商品：后端接收前端的商品信息的数据，并储存'''
    #判断登陆
    username = session.get('username')
    if not username:
        flash(u"请先登录！", "danger")
        return redirect(url_for("Home.home"))

    #通过用户名查找该用户的信息
    user = User.query.filter(User.username == username).first()

    #该用户没有进行身份验证：给出提示并返回个人主页
    if not user.identity_ok:
        flash(u"未通过身份认证，请先进行认证！", "danger")
        return redirect(url_for("user.panel"))
    
    if request.method == 'POST':
        f = request.files.get('img')
        #没有上传商品的图片，给出提示并返回商品添加页
        if not f:
            flash(u"未上传图片！", 'warning')
            return render_template('add_prod.html', username=username)
        f.filename = f.filename.lower()
        #上传商品的图片的类型有误，给出提示并返回商品添加页
        if not (f and allowed_file(f.filename)):
            flash(u"请检查上传的图片类型，仅限于png, jpg, bmp", "warning")
            return redirect(url_for('store.add_product'))
        #商品图片文件保存到相应目录下
        basepath = os.path.dirname(__file__)
        ext = os.path.splitext(f.filename)[1]
        new_filename = str(uuid.uuid1()) + ext
        # 注意：没有的文件夹一定要先创建，不然会提示没有该路径
        upload_path = os.path.join(basepath, '../static/store_images',
                                   new_filename)
        f.save(upload_path) 
        if not (user.identity_ok):
            flash(u"未进行身份认证，请先完成认证！", "error")
            return redirect(url_for('user.panel')) 
        #根据信息创建商品对象，并保存进数据库中
        #给出提示，并进入我的商品界面
        product = Product(
            name=request.form.get('name'),
            desc=request.form.get('desc'),
            label=request.form.get('label'),
            price=request.form.get('price'),
            img='store_images/' + new_filename,
            uploader_name=user.username,
            uploader_email=user.email,  
            update_admin=True,
            identity_ok=False
        )
        db.session.add(product)
        db.session.commit()
        flash(u"商品添加成功！", "success")
        return redirect(url_for('Check_store.my_prod'))
    return render_template('add_prod.html', username=username)

# 2.删除商品
@store_app.route('/delprod', methods=['GET', 'POST'])
@login_required
def del_prod():
    '''删除商品：删除数据库中的对象，以及聊天记录，并将本地的图片删除'''
    prod_del_id = request.args.get('prod_del_id')
    prod_del = Product.query.filter_by(id=prod_del_id).first()
    #若该商品不是本用户的商品：给出提示，返回商品列表
    if prod_del.uploader_name != session.get('username'):
        flash(u"不是本人的商品", 'danger')
        return redirect(url_for('Check_store.my_prod'))
    # 获取要删除的本地图片的绝对路径
    base_path = os.path.dirname(os.path.dirname(__file__))
    abs_path = os.path.join(base_path, 'static/store_images',
                            os.path.basename(prod_del.img))  
    # 删除数据库中的对象
    db.session.delete(prod_del)
    db.session.commit()
    # 删除本地保存的图片
    if os.path.exists(abs_path):
        os.remove(abs_path)
    # 删除相关商品的所有聊天记录
    msg_del_list = Message.query.filter_by(related_product_id=prod_del_id).all()
    for msg in msg_del_list:
        db.session.delete(msg)
    db.session.commit()
    return redirect(url_for('Check_store.my_prod'))

# 3.修改商品信息
@store_app.route('/modifyprod/<prod_id>', methods=['GET', 'POST'])
@login_required
def modify_prod(prod_id):
    '''
    修改商品信息：首先进行身份和商品判断，
    然后更改商品的图片、名称、描述、标签等，并将商品的状态改为等待审核
    '''
    prod_modify = Product.query.filter_by(id=prod_id).first()
    #商品不存在：给出提示，返回商品列表页面
    if not prod_modify:
        flash(u"不存在的商品", 'warning')
        return redirect(url_for('Check_store.my_prod'))  
    #商品不属于本用户：给出提示，返回商品列表页面
    if prod_modify.uploader_name != session.get('username'):
        flash(u"不是本人的商品", 'danger')
        return redirect(url_for('Check_store.my_prod'))
    if request.method == 'POST':
        # 获取要删除的本地图片的绝对路径
        base_path = os.path.dirname(os.path.dirname(__file__))
        abs_path = os.path.join(base_path, 'static/store_images',
                                os.path.basename(prod_modify.img))   
        #将新图片保存到相应路径下
        f = request.files.get('img')
        if f:
            #图片类型有误，给出提示并返回修改信息界面
            if not allowed_file(f.filename):
                error = "请检查上传的图片类型，仅限于png, jpg, bmp"
                return render_template('modify_prod.html', username=session.get('username'), prod=prod_modify)
            #保存图片
            else:
                basepath = os.path.dirname(__file__)
                ext = os.path.splitext(f.filename)[1]
                new_filename = str(uuid.uuid1()) + ext
                upload_path = os.path.join(basepath, '../static/store_images',
                                           new_filename)
                f.save(upload_path)
                prod_modify.img = 'store_images/' + new_filename
                if os.path.exists(abs_path):
                    os.remove(abs_path)
        #获取需要修改的信息，并进行修改后保存至数据库中
        prod_modify.name = request.form.get('name')
        prod_modify.desc = request.form.get('desc')
        prod_modify.label = request.form.get('label')
        prod_modify.price = request.form.get('price')
        prod_modify.update_admin = True
        prod_modify.identity_ok = False
        db.session.commit()
        flash(u"商品修改成功，请等待重新审核", 'success')
        return redirect(url_for('Check_store.my_prod'))
    return render_template('modify_prod.html', username=session.get('username'), prod=prod_modify)