# 任何测试的用例函数，需以test_作为前缀
# 测试方法是，在项目目录（而非test目录）下pytest -v
# 需要首先install pytest库

# 建议测试的时候少测试或不测试那个reset（在那个test前面加上字母前缀），不要重复给ljl邮箱发邮件，烦死啦烦死啦烦死啦！！

import os
import tempfile
import pytest


from io import BytesIO

import app

# 产生一个client固件
@pytest.fixture
def client():
    with app.app.test_client() as client:
        ## 这里每次执行一个测试用例的时候，都先把数据库清空
        with app.app.app_context():
            app.db.drop_all()
            app.create_db()
        yield client


# 测试数据库为空的时候，是否有商品
def test_empty_db(client):
    """Start with a blank database."""

    rv = client.get('/')
    str = '暂时还没有商品'
    str = str.encode()
    assert str in rv.data


# 测试注册
## 首先写一个注册
def regist(client, username, password1, password2, email):
    return client.post('/regist',data=dict(username=username,password1=password1,password2=password2,email=email),follow_redirects=True)
## 测试注册
def test_regist(client):
    ### 注册成功的情况
    str = '注册成功'
    rv = regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    str = str.encode()
    assert str in rv.data
    ### 两次密码不同的注册情况
    str = '两次密码不相同'
    rv = regist(client, 'lxy', '666666', '777777', 'lijialin0222@163.com')
    str = str.encode()
    assert str in rv.data
    ### 与已有账号用户名相同的注册情况
    str = '该用户名或邮箱已被注册'
    rv = regist(client, 'ljl', '666666', '666666', 'lijialin0222@163.com')
    str = str.encode()
    assert str in rv.data
    ### 与已有账号邮箱相同的注册情况
    str = '该用户名或邮箱已被注册'
    rv = regist(client, 'lxy', '666666', '666666', '1021278241@qq.com')
    str = str.encode()
    assert str in rv.data
    ### 用户名为空的情况
    str = '用户名长度必须在1-30个字符内'
    rv = regist(client, '', '666666', '666666', 'lijialin0222@163.com')
    str = str.encode()
    assert str in rv.data
    ### 密码为空的情况
    str = '密码长度必须在6-30个字符内'
    rv = regist(client, 'lxy', '', '', 'lijialin0222@163.com')
    str = str.encode()
    assert str in rv.data
    ### 邮箱格式错误的情况
    str = '邮箱格式不正确'
    rv = regist(client, 'lxy', '666666', '666666', 'lbynb')
    str = str.encode()
    assert str in rv.data
    ### 使用管理员账号的情况
    str = '该用户名或邮箱已被注册'
    rv = regist(client, 'admin1', '666666', '666666', 'lijialin0222@163.com')
    str = str.encode()
    assert str in rv.data
    

# 测试登录与注销
## 首先写一个登录
def login(client, username, password):
    return client.post('/login',data=dict(username=username,password=password),follow_redirects=True)
## 首先写一个注销
def logout(client):
    return client.get('/logout',follow_redirects=True)
## 测试登录和注销
def test_login_and_logout(client):
    ### 首先注册
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    ### 测试登录成功
    rv = login(client, 'ljl','111111')
    str = '成功登录'
    str = str.encode()
    assert str in rv.data
    ### 测试注销成功
    rv = logout(client)
    str = '已注销'
    str = str.encode()
    assert str in rv.data
    ### 测试用户名不存在
    rv = login(client, 'lxy','111111')
    str = '错误的用户名或密码'
    str = str.encode()
    assert str in rv.data
    ### 测试密码错误
    rv = login(client, 'ljl','666666')
    str = '错误的用户名或密码'
    str = str.encode()
    assert str in rv.data

# 测试修改密码
## 修改密码
def change(client, password0, password1, password2):
    return client.post('/change', data=dict(password0=password0, password1=password1, password2=password2),follow_redirects=True)
## 测试修改密码
def test_change(client):
    ### 首先注册一个用户，并且登录
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')
    ### 测试原密码错误
    rv = change(client, '123123','666666', '666666')
    str = '原密码错误'
    str = str.encode()
    assert str in rv.data
    ### 测试两次新密码不同
    rv = change(client, '111111','666666', '777777')
    str = '两次密码输入不相同'
    str = str.encode()
    assert str in rv.data
    ### 测试新密码为空
    rv = change(client, '111111','', '')
    str = '密码长度必须在6-30个字符内'
    str = str.encode()
    assert str in rv.data

# 测试找回密码
##忘记密码
def reset(client, name, email):
    return client.post('/reset', data=dict(name=name,email=email),follow_redirects=True)
## 找回密码
def reset_(client, password1, password2):
    return client.post('/reset_/<token>', data=dict(password1=password1, password2=password2),follow_redirects=True)
def test_reset(client):
    ### 首先注册一个账号
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    ### 用户名错误
    rv = reset(client, 'lxy','1021278241@qq.com')
    str = '用户名或邮箱输入错误'
    str = str.encode()
    assert str in rv.data
    ### 邮箱错误
    rv = reset(client, 'ljl','lijialin0222@163.com')
    str = '用户名或邮箱输入错误'
    str = str.encode()
    assert str in rv.data
    ### 忘记密码成功
    rv = reset(client, 'ljl','1021278241@qq.com')
    str = '成功发送验证邮件'
    str = str.encode()
    assert str in rv.data
    ### 测试找回密码，两次密码不相同
    ### 这里应该有东西的，但是我不会测试动态的URL

# 这里应该是测试的身份认证，但是由于图片出了问题，不会测了。。
# 测试身份认证
## 身份认证
def identity(client, file):
    return client.post('/identity',data=dict(file),content_type='multipart/form-data',follow_redirects=True)
## 测试身份认证
def test_identity(client):
    ### 首先注册一个用户，并且登录
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')

    ### 上传成功
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    rv = identity(client, file)
    str = '上传成功'
    str = str.encode()
    assert str in rv.data

    file = {'file': (BytesIO(b"abcdef"), 'test.png')}
    rv = identity(client, file)
    str = '上传成功'
    str = str.encode()
    assert str in rv.data

    file = {'file': (BytesIO(b"abcdef"), 'test.bmp')}
    rv = identity(client, file)
    str = '上传成功'
    str = str.encode()
    assert str in rv.data

    ### 上传失败
    file = {'file': (BytesIO(b"abcdef"), 'test.pdf')}
    rv = identity(client, file)
    str = '请检查上传的图片类型，仅限于png、jpg、bmp'
    str = str.encode()
    assert str in rv.data

# 测试修改用户名
## 修改用户名
def changeName(client, username):
    return client.post('/change_name', data=dict(username=username), follow_redirects=True)
def test_changeName(client):
    ### 首先注册一个用户，并且登录
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    regist(client, 'lxy', '111111', '111111', 'lijialin0222@163.com')
    login(client, 'ljl','111111')
    ### 测试修改用户名重名的情况
    rv = changeName(client, 'lxy')
    str = '用户名已存在'
    str = str.encode()
    assert str in rv.data
    ### 测试修改的用户名是管理员用户名的时候
    rv = changeName(client, 'admin1')
    str = '用户名已存在'
    str = str.encode()
    assert str in rv.data
    ### 测试修改的用户名为空的情况
    rv = changeName(client, '')
    str = '用户名长度必须在1-30个字符内'
    str = str.encode()
    assert str in rv.data
    ### 测试修改成功的时候
    rv = changeName(client, 'ljl666')
    str = '昵称修改成功'
    str = str.encode()
    assert str in rv.data

# 测试封号部分
## 封号
def admin_account(client, username, is_pass):
    return client.post('/admin/account', data=dict(username=username,is_pass=is_pass), follow_redirects=True)
def test_admin_account(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'admin1','password1')
    ### 测试封号成功
    rv = admin_account(client, 'ljl', 'yes')
    str = '封号成功'
    str = str.encode()
    assert str in rv.data
    ### 测试重复封号
    rv = admin_account(client, 'ljl', 'yes')
    str = '该账号已被封禁'
    str = str.encode()
    assert str in rv.data
    ### 测试解除封号成功
    rv = admin_account(client, 'ljl', 'no')
    str = '解封成功'
    str = str.encode()
    assert str in rv.data
    ### 测试重复解除封号
    rv = admin_account(client, 'ljl', 'no')
    str = '该账号未被封禁'
    str = str.encode()
    assert str in rv.data


# 测试通过审核
## 通过审核
def admin_examine(client, username, is_pass, reason1):
    return client.post('/admin/examine/'+username, data=dict(is_pass=is_pass, reason1=reason1), follow_redirects=True)
def test_admin_examine(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl', '111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}

    rv = identity(client, file)
    str = '上传成功'
    str = str.encode()
    assert str in rv.data
    logout(client)
    login(client, 'admin1', 'password1')

    ## 测试通过审核
    rv = admin_examine(client, 'ljl', 'yes', '111')
    str = '已通过'
    str = str.encode()
    assert str in rv.data

    ## 测试不通过审核
    rv = admin_examine(client, 'ljl', 'no', '111')
    str = '已拒绝通过'
    str = str.encode()
    assert str in rv.data


# 测试修改头像
## 修改头像
def changeHead(client, file):
    return client.post('/change_head', data=dict(file), content_type='multipart/form-data', follow_redirects=True)
def test_changeHead(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    rv = changeHead(client, file)
    str = '上传成功！'
    str = str.encode()
    assert str in rv.data


# 测试添加商品
## 添加商品
def add_product(client, name, label, desc, price, img):
    return client.post('/add_product', data=dict(name=name,label=label,desc=desc,price=price,img=img['file']), content_type='multipart/form-data', follow_redirects=True)
def test_add_product(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')

    ### 测试未认证时上传
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    rv = add_product(client, '1', '1', '1', '1', img)
    str = '个人中心' # 这里是因为紧接着有一个跳转，而且因为跳转的存在，无法直接检测flash信息，采取了一个折中的方法检查是否跳回到panel
    str = str.encode()
    assert str in rv.data

    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 测试商品添加成功
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    rv = add_product(client, '1', '1', '1', '1', img)
    str = '商品添加成功！'
    str = str.encode()
    assert str in rv.data

    ### 测试未上传图片
    img = {'file': ''}
    rv = add_product(client, '1', '1', '1', '1', img)
    str = '未上传图片！'
    str = str.encode()
    assert str in rv.data

    ### 测试上传图片格式错误
    img = {'file': (BytesIO(b"abcdef"), 'pic.pdf')}
    rv = add_product(client, '1', '1', '1', '1', img)
    str = '请检查上传的图片类型，仅限于png, jpg, bmp'
    str = str.encode()
    assert str in rv.data

# 测试审核商品
## 审核商品
def admin_picture(client, product_name, is_pass):
    return client.post('/admin/picture/'+product_name, data=dict(is_pass=is_pass), follow_redirects=True)
def test_admin_picture(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 先添加商品
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '1', '1', '1', '1', img)
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '2', '2', '2', '2', img)

    ### 测试通过商品审核
    logout(client)
    login(client, 'admin1', 'password1')
    rv = admin_picture(client, '1', 'yes')
    str = '已通过'
    str = str.encode()
    assert str in rv.data

    ### 测试不通过商品审核
    logout(client)
    login(client, 'admin1', 'password1')
    rv = admin_picture(client, '2', 'no')
    str = '已拒绝通过'
    str = str.encode()
    assert str in rv.data

# 测试查看我的商品
## 我的商品
def my_prod(client):
    return client.post('/myprod', follow_redirects=True)
def test_my_prod(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 先添加商品
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '1', '1', '1', '1', img)
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '2', '2', '2', '2', img)
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '3', '3', '3', '3', img)

    ### 再去审核一下
    logout(client)
    login(client, 'admin1', 'password1')
    admin_picture(client, '2', 'no')
    admin_picture(client, '3', 'yes')

    ### 去我的商品那里看看有没有
    logout(client)
    login(client, 'ljl','111111')
    rv = my_prod(client)
    str = '商品名称：1'
    str = str.encode()
    assert str in rv.data
    str = '价格：1'
    str = str.encode()
    assert str in rv.data
    str = '等待审核'
    str = str.encode()
    assert str in rv.data

    rv = my_prod(client)
    str = '商品名称：2'
    str = str.encode()
    assert str in rv.data
    str = '价格：2'
    str = str.encode()
    assert str in rv.data
    str = '审核未通过，请进行修改'
    str = str.encode()
    assert str in rv.data

    rv = my_prod(client)
    str = '商品名称：3'
    str = str.encode()
    assert str in rv.data
    str = '价格：3'
    str = str.encode()
    assert str in rv.data

# 测试查看商品详细信息
## 查看商品详细信息
def show_item(client, keyword, item_id):
    return client.post('/show_item/'+keyword+'/'+item_id, follow_redirects=True)
def test_show_item(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 先添加商品
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '1', '1', '1', '1', img)

    ### 再去审核一下
    logout(client)
    login(client, 'admin1', 'password1')
    admin_picture(client, '1', 'yes')

    ### 再去查看一下
    logout(client)
    login(client, 'ljl','111111')
    rv = show_item(client, '0', '1')
    
    str = '返回主页'
    str = str.encode()
    assert str in rv.data
    str = '添加收藏'
    str = str.encode()
    assert str in rv.data

# 测试查看浏览记录
## 查看浏览记录
def my_browse(client):
    return client.post('/my_browse', follow_redirects=True)
def test_my_browse(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 先添加商品
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '1', '1', '1', '1', img)

    ### 再去审核一下
    logout(client)
    login(client, 'admin1', 'password1')
    admin_picture(client, '1', 'yes')

    ### 再去查看一下
    logout(client)
    login(client, 'ljl','111111')
    show_item(client, '0', '1')

    ### 再去浏览记录检查一下
    rv = my_browse(client)
    str = '浏览记录1：'
    str = str.encode()
    assert str in rv.data
    str = '商品名称：1'
    str = str.encode()
    assert str in rv.data
    str = '价格：1'
    str = str.encode()
    assert str in rv.data

## 测试删除浏览记录
### 删除浏览记录
#def del_my_browse(client, sel_prod):
#    return client.post('/edit_my_collect', data=dict(sel_prod=sel_prod), follow_redirects=True)
#def test_del_my_browse(client):
#    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
#    login(client, 'ljl','111111')
#    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
#    identity(client, file)
#    logout(client)
#    login(client, 'admin1', 'password1')
#    admin_examine(client, 'ljl', 'yes', '111')
#    logout(client)
#    login(client, 'ljl','111111')
#
#    ### 先添加商品
#    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
#    add_product(client, '1', '1', '1', '1', img)
#
#    ### 再去审核一下
#    logout(client)
#    login(client, 'admin1', 'password1')
#    admin_picture(client, '1', 'yes')
#
#    ### 再去查看一下
#    logout(client)
#    login(client, 'ljl','111111')
#    show_item(client, '0', '1')
#
#    ### 再删除浏览记录
#    rv = del_my_browse(client, {1})
#    str = '您暂时没有浏览记录'
#    str = str.encode()
#    assert str in rv.data

# 测试添加收藏
## 添加收藏
def add_collect(client, item_id):
    return client.post('/add_my_collect/'+item_id, follow_redirects=True)    
## 浏览收藏记录
def my_collect(client):
    return client.post('/my_collect', follow_redirects=True)
def test_my_collect(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 先添加商品
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '1', '1', '1', '1', img)

    ### 再去审核一下
    logout(client)
    login(client, 'admin1', 'password1')
    admin_picture(client, '1', 'yes')

    ### 再去查看一下
    logout(client)
    login(client, 'ljl','111111')
    show_item(client, '0', '1')

    ### 再去收藏一下
    rv = add_collect(client, '1');
    str = '已收藏'
    str = str.encode()
    assert str in rv.data

    ### 再去查看一下收藏夹
    rv = my_collect(client)
    str = '收藏商品：'
    str = str.encode()
    assert str in rv.data
    str = '商品名称：1'
    str = str.encode()
    assert str in rv.data
    str = '价格：1'
    str = str.encode()
    assert str in rv.data

# 测试聊天
def chat(client, product_id, user_id, chat_partner_id, Text):
    return client.post('/chat/'+user_id+'/'+chat_partner_id+'/'+product_id, data=dict(Text=Text), follow_redirects=True)
def test_chat(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    regist(client, 'lxy', '111111', '111111', 'lijialin0222@163.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 先添加商品
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '1', '1', '1', '1', img)

    ### 再去审核一下
    logout(client)
    login(client, 'admin1', 'password1')
    admin_picture(client, '1', 'yes')

    ### 再去聊天一下
    logout(client)
    login(client, 'lxy','111111')
    rv = chat(client, '1', '3', '2', 'lxy拍了拍你')
    str = 'lxy拍了拍你'
    str = str.encode()
    assert str in rv.data
    str = '您与ljl的聊天内容如下'
    str = str.encode()
    assert str in rv.data

# 测试消息列表
## 消息列表
def msg_list(client):
    return client.get('/message_list')
def test_msg_list(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    regist(client, 'lxy', '111111', '111111', 'lijialin0222@163.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 先添加商品
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '1', '1', '1', '1', img)

    ### 再去审核一下
    logout(client)
    login(client, 'admin1', 'password1')
    admin_picture(client, '1', 'yes')

    ### 再去聊天一下
    logout(client)
    login(client, 'lxy','111111')
    chat(client, '1', '3', '2', 'lxy拍了拍你')
    
    ### 这个时候去看看消息列表
    rv = msg_list(client)
    str = '相关聊天如下'
    str = str.encode()
    assert str in rv.data
    str = '与商品【1】相关的聊天:'
    str = str.encode()
    assert str in rv.data
    str = '与用户“ljl”的聊天记录:'
    str = str.encode()
    assert str in rv.data

# 测试消息历史
## 消息历史
def chat_records(client, user_id, chat_partner_id, product_id, location):
    return client.post('/chat_records/'+user_id+'/'+chat_partner_id+'/'+product_id+'/'+location, follow_redirects=True)
def test_chat_records(client):
    regist(client, 'ljl', '111111', '111111', '1021278241@qq.com')
    regist(client, 'lxy', '111111', '111111', 'lijialin0222@163.com')
    login(client, 'ljl','111111')
    file = {'file': (BytesIO(b"abcdef"), 'test.jpg')}
    identity(client, file)
    logout(client)
    login(client, 'admin1', 'password1')
    admin_examine(client, 'ljl', 'yes', '111')
    logout(client)
    login(client, 'ljl','111111')

    ### 先添加商品
    img = {'file': (BytesIO(b"abcdef"), 'pic.png')}
    add_product(client, '1', '1', '1', '1', img)

    ### 再去审核一下
    logout(client)
    login(client, 'admin1', 'password1')
    admin_picture(client, '1', 'yes')

    ### 再去聊天一下
    logout(client)
    login(client, 'lxy','111111')
    chat(client, '1', '3', '2', 'lxy拍了拍你')

    ### 这个时候看看消息记录
    rv = chat_records(client, '3', '2', '1', '0');
    str = '聊天记录如下'
    str = str.encode()
    assert str in rv.data

    str = 'lxy拍了拍你'
    str = str.encode()
    assert str in rv.data
