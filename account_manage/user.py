from account_manage.account_models import User
from chat_manage.chat_models import Message
from db_manage.sql import db
from flask import render_template,flash, session, Blueprint
from account_manage.login import login_required
import sys

sys.path.append("..")

# 跨文件路由需要蓝图
account_app = Blueprint('user', __name__)

#===========================================================================
#路由
# 1.个人中心
@account_app.route('/panel')
@login_required
def panel():
    '''个人中心：后端向前端返回用户信息在前端显示'''
    username = session.get('username')
    user = User.query.filter(User.username == username).first()
    # 检查是否有未读消息
    all_unread_list = Message.query.filter_by(
        receiver_id=user.id, already_read=False).all()
    if len(all_unread_list) == 0:
        user.have_unread_messages = False
    else:
        user.have_unread_messages = True
        flash(u'您有新的未读消息！', 'info')
    db.session.commit()
    return render_template("panel.html",
                           user=user,
                           username=session.get('username'))