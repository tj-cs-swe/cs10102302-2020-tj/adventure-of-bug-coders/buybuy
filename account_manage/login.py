from account_manage.account_models import User
from db_manage.sql import db
from functools import wraps
from sqlalchemy import and_, or_
from flask import request, render_template, redirect, url_for, flash, session, Blueprint
import sys
import re
import hashlib

sys.path.append("..")

# 跨文件路由需要蓝图
login_app = Blueprint('Login', __name__)

#==========================================================
# 辅助函数
# 1.登录检验（用户名、密码验证）
def valid_login(username, hashkey):
    user = User.query.filter(
        and_(User.username == username, User.hashkey == hashkey)).first()
    if user:
        if user.is_ban == False:
            return 1
        else:
            return 2
    else:
        return 3

# 2.注册检验（用户名、邮箱验证）
def valid_regist(username, email):
    user = User.query.filter(
        or_(User.username == username, User.email == email)).first()
    if user:
        return False
    else:
        return True

# 3.登录
def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if session.get('username'):
            return func(*args, **kwargs)
        else:
            return redirect(url_for('Login.login', next=request.url))  #

    return wrapper
#===========================================================
#路由
# 1.登录
@login_app.route('/login', methods=['GET', 'POST'])
def login():
    '''登录：检验账号密码是否匹配'''
    if request.method == 'POST':
        #获取输入的用户名和密码
        Password = request.form['password']
        hash = hashlib.sha256()
        hash.update('buybuy'.encode('utf-8'))
        hash.update(Password.encode('utf-8'))
        hashkey = hash.hexdigest() 
        #用户名与密码对应的键值匹配，成功登录
        if valid_login(request.form['username'], hashkey) == 1:
            flash(u"成功登录！", 'success')
            session['username'] = request.form.get('username')
            user = User.query.filter(
                User.username == request.form.get('username')).first()
            if user and user.admin_level:
                return redirect(url_for('user.panel'))
            return redirect(url_for('Home.home'))   
        else:
        #用户名与密码对应的键值不匹配    
            if valid_login(request.form['username'], hashkey) == 3:
                flash(u'错误的用户名或密码！', 'danger')
        #账号被封禁
            else:
                flash(u'账号被封禁，请联系管理员！', 'danger')
    return render_template('login.html')

# 2.注销
@login_app.route('/logout')
def logout():
    '''注销：在session中pop信息'''
    session.pop('username', None)
    flash(u"已注销", 'success')
    return redirect(url_for('Home.home'))

# 3.注册
@login_app.route('/regist', methods=['GET', 'POST'])
def regist():
    '''注册：后端接收前端注册信息，进行数据合法性判断，并返回错误信息'''
    if request.method == 'POST':
        #对用户名的格式进行限制
        if len(request.form['username']) == 0 or len(request.form['username']) > 30:
            flash(u'用户名长度必须在1-30个字符内', 'danger')
            return render_template('regist.html')
        if len(request.form['password1']) < 6 or len(request.form['password1']) > 30:
            flash(u'密码长度必须在6-30个字符内', 'danger')
            return render_template('regist.html')     
        #对邮箱的格式进行限制以及合法性检测
        E_mail = request.form['email']
        if re.match(r'^[0-9a-zA-Z_]{0,19}@[0-9a-zA-Z]{1,13}(\.edu)?\.[com,cn,net]{1,3}$', E_mail):
            E_mail = E_mail
        else:
            flash(u'邮箱格式不正确', 'danger')
            return render_template('regist.html')
        if request.form['password1'] != request.form['password2']:
            flash(u'两次密码不相同！', 'danger')
        elif valid_regist(request.form['username'], request.form['email']):
            hash = hashlib.sha256()
            hash.update('buybuy'.encode('utf-8'))  # salt
            hash.update(request.form['password1'].encode('utf-8'))
            user = User(username=request.form['username'],
                        hashkey=hash.hexdigest(),
                        email=request.form['email'],
                        identity_ok=False,
                        update_identity=False,
                        head='../static/head_images/start.jpg',
                        is_ban=False,
                        admin_level=0)
            db.session.add(user)
            db.session.commit()
            flash(u"注册成功！", 'success')
            return redirect(url_for('Login.login'))
        else:
            flash(u'该用户名或邮箱已被注册！', 'danger')
    return render_template('regist.html')