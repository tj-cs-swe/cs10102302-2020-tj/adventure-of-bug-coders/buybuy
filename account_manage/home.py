from store_manage.store_models import Product
from flask import request, render_template, session, Blueprint, current_app
import sys
import random

sys.path.append("..")

# 跨文件路由需要蓝图
home_app = Blueprint('Home', __name__)
#======================================================
#路由
# 1. 返回ico图标
@home_app.route('/favicon.ico')
def favicon():
    '''后端返回文件给前端（浏览器），send_static_file是Flask框架自带的函数'''
    return current_app.send_static_file('img/favicon.png')

# 2.主页
@home_app.route('/')
def home():
    '''主页：后端将数据库中的商品返回给前端'''
    product = Product.query.filter_by(identity_ok=True).all()  # 从服务器取得数据
    list = []
    length = len(product)
    per_page = 12  # 每页显示十二个商品
    page = int(request.args.get('page', 1))  # 获取页码
    paginate = Product.query.filter_by(identity_ok=True).paginate(
        page, per_page, error_out=False)  # 创建分页器对象
    page_product = paginate.items
    #添加最多5件商品信息进入list
    if length > 5:
        for i in range(0, 5, 1):
            while 1:
                r = random.randint(0, length - 1)
                if product[r] in list:
                    continue
                else:
                    break
            list.append(product[r])
    else:
        list = product
    return render_template('home.html',
                           username=session.get('username'),
                           product=product,
                           list=list,
                           page_product=page_product,
                           paginate=paginate)