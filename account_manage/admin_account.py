from store_manage.store_models import Product
from account_manage.account_models import User
from db_manage.sql import db
from flask import request, render_template, redirect, url_for, flash, session, Blueprint
from account_manage.login import login_required
import os
import sys
from datetime import timedelta
import uuid

sys.path.append("..")

# 跨文件路由需要蓝图
admin_account_app = Blueprint('Admin_account', __name__)

#======================================================
#辅助函数
# 1.0设置允许的文件格式
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'JPG', 'PNG', 'bmp'])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

# 1.1图片上传相关方法，用于身份认证和修改头像
def upload_pic(username, choice):
    user = User.query.filter(User.username == username).first()
    f = request.files['file']
    if not (f and allowed_file(f.filename)):
        flash(u"请检查上传的图片类型，仅限于png、jpg、bmp", 'warning')
        return 0
    else:
        # 路径获取
        basepath = os.path.dirname(__file__)
        ext = os.path.splitext(f.filename)[1]
        new_filename = str(uuid.uuid1()) + ext
        # 注意：没有的文件夹一定要先创建，不然会提示没有该路径
        if choice == 1:       
            upload_path = os.path.join(basepath, '../static/identity_images',
                                new_filename)
        elif choice == 2:
            upload_path = os.path.join(basepath, '../static/head_images',
                                new_filename)
        # 保存图片
        f.save(upload_path)  
        # 删除原来的图片 
        if choice==1:
            if user.identity:  
                del_path = os.path.join(basepath, '../static/', user.identity)
                if os.path.exists(del_path):
                    os.remove(del_path)
            user.identity = 'identity_images/' + new_filename
            user.update_identity = True
        elif choice==2:
            if user.head and user.head != '../static/head_images/start.jpg':             
                del_path = os.path.join(basepath, '../static/', user.head)
                if os.path.exists(del_path):
                    os.remove(del_path)
            user.head = 'head_images/' + new_filename
        db.session.commit()
        flash(u'上传成功！', 'success')
        return choice

# 1.2设置静态文件缓存过期时间
admin_account_app.send_file_max_age_default = timedelta(seconds=1)

#======================================================
#路由
# 0用户上传图片
@admin_account_app.route('/identity', methods=['POST', 'GET'])
@login_required
def identity():
    '''上传图片：前端上传用户认证图片'''
    username = session.get('username')
    if request.method == 'POST':
        if upload_pic(username,1)==1:
            return redirect(url_for('user.panel'))
    return render_template('identity.html', username=username)

# 1审核用户
@admin_account_app.route('/admin/examine', methods=['POST', 'GET'])
@admin_account_app.route('/admin/examine/<getname>', methods=['POST', 'GET'])
@login_required
def admin_examine(getname=""):
    '''审核用户：管理员进行用户审核，后断接收前端返回的审核结果，并保存用户信息'''
    cur_user = User.query.filter(
        User.username == session.get('username')).first()
    #不是管理员账号：给出提示并跳转至登录页面
    if not (cur_user and cur_user.admin_level):
        flash(u'请使用管理员账号登录！', 'danger')
        return redirect(url_for('Login.login'))
    if request.method == 'POST':
        #在数据库中查找该账号
        user = User.query.filter(User.username == getname).first()
        #修改用户的审核状态
        if user:
            if request.form.get('is_pass') == 'no':
                user.identity_ok = False
                user.identity_reason = request.form.get('reason1')
                flash(u'已拒绝通过', 'success')
            elif request.form.get('is_pass') == 'yes':
                user.identity_ok = True
                flash(u'已通过', 'success')
            user.update_identity = False
            db.session.commit()
        return redirect(url_for('Admin_account.admin_examine'))
    #寻找所有的更新过信息的用户
    users_get = User.query.all()
    users = []
    for uu in users_get:
        if uu.update_identity:  
            users.append(uu)
    return render_template('admin_examine.html', users=users, username=session.get('username'))

# 2 账号封禁与解封
@admin_account_app.route('/admin/account', methods=['POST', 'GET'])
@login_required
def admin_account():
    '''账号封禁：管理员进行封号操作'''
    cur_user = User.query.filter(
        User.username == session.get('username')).first()
    #不是管理员账号：给出提示并跳转至登录页面
    if not (cur_user and cur_user.admin_level):
        flash(u'请使用管理员账号登录！', 'danger')
        return redirect(url_for('Login.login'))
    if request.method == 'POST':
        Username = request.form['username']
        #在数据库中查找要操作的账号
        user = User.query.filter(User.username == Username).first()
        if user:
            #由于等级问题，无权封禁或解封该账号
            if user.admin_level >= cur_user.admin_level:
                flash(u'无权封禁或解封该账号！请联系主管理员', 'danger')
            #解封账号
            elif request.form.get('is_pass') == 'no':
                if user.is_ban == False:#该账号本就未被封禁
                    flash(u'该账号未被封禁', 'danger')
                else:
                    flash('解封成功', 'success')
                    user.is_ban = False
                    db.session.commit()
            #封禁账号
            elif request.form.get('is_pass') == 'yes':
                if user.is_ban == True:#该账号本就已被封禁
                    flash(u'该账号已被封禁', 'danger')
                else:
                    flash('封号成功', 'success')
                    user.is_ban = True
                    db.session.commit()
        #账号不存在
        else:
            flash(u'该账号不存在', 'danger')
    return render_template('admin_account.html', username=session.get('username'))


# 3.审核商品
@admin_account_app.route('/admin/picture', methods=['POST', 'GET'])
@admin_account_app.route('/admin/picture/<getname>', methods=['POST', 'GET'])
@login_required
def admin_picture(getname=""):
    cur_user = User.query.filter(
        User.username == session.get('username')).first()
    #不是管理员账号：给出提示并跳转至登录页面
    if not (cur_user and cur_user.admin_level):
        flash(u'请使用管理员账号登录！', 'danger')
        return redirect(url_for('Login.login'))
    if request.method == 'POST':
        #在数据库中查找该商品
        prod = Product.query.filter(Product.id == getname).first()
        #修改商品审核状态
        if prod:
            if request.form.get('is_pass') == 'no':
                prod.identity_ok = False
                flash(u'已拒绝通过', 'success')
                prod.identity_reason = request.form.get('reason1')
            elif request.form.get('is_pass') == 'yes':
                prod.identity_ok = True
                flash(u'已通过', 'success')
            prod.update_admin = False
            db.session.commit()
        return redirect(url_for('Admin_account.admin_picture'))
    prods_get = Product.query.all()
    prods = []
     # 寻找所有的更新过信息的商品
    for uu in prods_get:
        if uu.update_admin: 
            prods.append(uu)
    return render_template('admin_picture.html', products=prods, username=session.get('username'))