from account_manage.account_models import User
from db_manage.sql import db
from flask import request, render_template, redirect, url_for, flash, session, Blueprint
from account_manage.login import login_required
from account_manage.admin_account import ALLOWED_EXTENSIONS
from account_manage.admin_account import allowed_file
from account_manage.admin_account import upload_pic
import os
import sys
import uuid

sys.path.append("..")

# 跨文件路由需要蓝图
change_info_app = Blueprint('Change_info', __name__)

#路由
# 1.修改昵称
@change_info_app.route('/change_name', methods=['GET', 'POST'])
@login_required
def change_name():
    '''修改昵称：后端接收前端的新昵称，并判断合法性'''
    if request.method == 'POST':
        username = request.form['username']
        user = User.query.filter(User.username == username).first()
        #用户名不允许重复
        if user:
            flash(u'用户名已存在', 'warning')
            return render_template('change_name.html', username=session.get('username'))
        #用户名格式限制
        if len(request.form['username']) == 0 or len(request.form['username']) > 30:
            flash(u'用户名长度必须在1-30个字符内', 'warning')
            return render_template('change_name.html', username=session.get('username'))
        username = session.get('username')
        #数据库中按用户名查询该账号，并进行修改
        user = User.query.filter(User.username == username).first()
        user.username = request.form['username']
        db.session.commit()
        flash("昵称修改成功", 'success')
        session['username'] = user.username
        return redirect(url_for('user.panel'))
    return render_template('change_name.html',
                           username=session.get('username'))
    
# 2.  修改头像
@change_info_app.route('/change_head', methods=['POST', 'GET'])
@login_required
def change_head():
    '''修改头像：接收新头像，并判断图片类型的合法性'''
    username = session.get('username')
    if request.method == 'POST':
        if upload_pic(username,2)==2:
            return redirect(url_for('user.panel'))
    return render_template('change_head.html', username=username)