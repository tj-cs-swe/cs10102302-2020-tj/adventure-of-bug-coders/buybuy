from account_manage import email
from account_manage.account_models import User
from db_manage.sql import db
from sqlalchemy import and_
from flask import request, render_template, redirect, url_for, flash, session, Blueprint
from account_manage.login import login_required
import sys
import hashlib
sys.path.append("..")

# 跨文件路由需要蓝图
password_app = Blueprint('Password', __name__)

#==========================================================
# 辅助函数
# 0.重置密码检验（用户名、邮箱验证）
def valid_reset(username, email):
    user = User.query.filter(
        and_(User.username == username, User.email == email)).first()
    if user:
        return False
    else:
        return True

#==========================================================
#路由
# 1.修改密码    
@password_app.route('/change', methods=['GET', 'POST'])
@login_required
def change():
    '''修改密码：后端接收前端的数据，并进行数据合法性检验'''
    if request.method == 'POST':
        hash = hashlib.sha256()
        hash.update('buybuy'.encode('utf-8'))
        hash0 = hashlib.sha256()
        hash0.update('buybuy'.encode('utf-8'))
        hash0.update(request.form['password0'].encode('utf-8'))
        #密码格式限制
        if len(request.form['password1']) < 6 or len(request.form['password1']) > 30:
            flash(u'密码长度必须在6-30个字符内', 'warning')
            return render_template('change.html', username=session.get('username'))
        #在数据库中查询账号
        username = session.get('username')
        user = User.query.filter(User.username == username).first()
        if user.hashkey == hash0.hexdigest():
            #两次密码相同
            if request.form['password1'] == request.form['password2']:
                #更新密码
                hash.update(request.form['password1'].encode('utf-8'))
                user.hashkey = hash.hexdigest()
                db.session.commit()
                flash(u"密码修改成功", 'success')
                return redirect(url_for('Login.login'))
            #两次密码不相同
            else:
                flash(u'两次密码输入不相同！', 'warning')
        #原密码错误
        else:
            flash(u'原密码错误', 'warning')
    return render_template('change.html',
                           username=session.get('username'))

# 2.0忘记密码
@password_app.route('/reset', methods=['GET', 'POST'])
def reset():
    '''忘记密码：向邮箱发送验证信息'''
    if request.method == 'POST':
        useremail = request.form['email']
        #验证用户名与邮箱是否匹配
        if valid_reset(request.form['name'], request.form['email']):
            flash(u'用户名或邮箱输入错误！', 'warning')
        else:
            #匹配，发送邮件
            user = User.query.filter(User.email == useremail).first()
            email.send_password_reset_email(user)
            flash(u'成功发送验证邮件！', 'success')
    return render_template('reset.html', username=session.get('username'))

# 2.1重置密码（也属于忘记密码当中的）
@password_app.route('/reset_/<token>', methods=['GET', 'POST'])
def reset_1(token):
    '''重置密码（属于忘记密码功能）：后端接受前端的密码，并进行合法性判断'''
    if request.method == 'POST':
        user = User.verify_jwt_token(token)
        #该账号是管理员账号，则不允许修改密码
        if not user:
            flash(u'这是管理员账号！', 'warning')
            print("串：", token)
            return redirect(url_for('Login.regist'))
        else:
            #设置新的密码
            #格式限制
            if len(request.form['password1']) < 6 or len(request.form['password1']) > 30:
                flash(u'密码长度必须在6-30个字符内', 'warning')
                return render_template('reset_.html', username=session.get('username'))
            #两次输入的密码是否相同
            if request.form['password1'] == request.form['password2']:
                hash = hashlib.sha256()
                hash.update('buybuy'.encode('utf-8'))
                hash.update(request.form['password1'].encode('utf-8'))
                user.hashkey = hash.hexdigest()
                db.session.commit()
                flash("密码修改成功", 'success')
                return redirect(url_for('Login.login'))
            else:
                flash(u'两次输入密码不相同!', 'warning')
    return render_template('reset_.html', username=session.get('username'))